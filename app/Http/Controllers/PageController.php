<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;
use App\Models\Page;
use App\Models\User;
use App\Notifications\FormNotify;

class PageController extends Controller
{
    public $single_page;
    
    public function render_page($page) {
        $agent = new Agent();
        $this->single_page = Page::where('url', $page)->first();
        return view('page',[
            'single_page'  => $this->single_page,
            'agent'           => $agent
            ]);
    }

    public function render_contact() {
        $agent = new Agent();
        $this->single_page = Page::where('url', 'contact')->first();
        return view('page',[
            'single_page'  => $this->single_page,
            'agent'           => $agent
            ]);
    }

    public function get_form_data(Request $req) {
        $form_data = $req->input();
        $req->validate([
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required',
            'captcha' => 'required|captcha',
        ]);
        $user = User::find(2);
        $user->notify(new FormNotify($form_data));
        return view('form-sucess');
    }
}
