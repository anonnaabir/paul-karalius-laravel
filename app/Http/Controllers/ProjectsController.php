<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;
use App\Models\Project;

class ProjectsController extends Controller
{
    
    public $single_project;
    
    public function render_projects() {
        $agent = new Agent();
        return view('projects', compact('agent'));
    }

    public function single_project($project_url) {
        $agent = new Agent();
        $this->single_project = Project::where('url', $project_url)->first();
        return view('single-project',[
            'single_project'  => $this->single_project,
            'project_name'  => $this->single_project->name,
            'agent'           => $agent
            ]);
    }
}
