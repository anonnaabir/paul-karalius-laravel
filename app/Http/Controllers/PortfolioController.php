<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;

class PortfolioController extends Controller {
    
    public function render_portfolio() {
        $agent = new Agent();
        return view('portfolio', compact('agent'));
    }

}
