<?php

namespace App\Http\Livewire;
use Request;
use Livewire\Component;
use App\Models\Portfolio;
use App\Models\Project;

class ImageGrid extends Component
{
    public $single_project;
    
    public function render()
    {
        // $portfolio = Portfolio::all();
        $portfolio = Portfolio::orderBy('order')->get();
        $projects = Project::all();
        $i = 1;

        if (isset($this->single_project)) {
            $project_images = json_decode($this->single_project->images, true);
            return view('livewire.image-grid', [
                'project_images'  => $project_images
            ]);
        }

        else {
            if (url()->current() == url('projects')) {
                $data_source = $projects ;
            }
    
            else {
                $data_source = $portfolio;
            }
            
            return view('livewire.image-grid', [
                'data_source' => $data_source,
                'index'           => $i,
                'request_url'  => Request::path()
            ]);
        }

        
    }
}
