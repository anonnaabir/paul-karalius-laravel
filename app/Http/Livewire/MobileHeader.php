<?php

namespace App\Http\Livewire;

use Livewire\Component;

class MobileHeader extends Component
{
    public function render()
    {
        return view('livewire.mobile-header');
    }
}
