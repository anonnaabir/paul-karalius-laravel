<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ImageCarousel extends Component
{
    public $single_project;
    
    public function render()
    {
        if (isset($this->single_project)) {
            $project_images = json_decode($this->single_project->images, true);
            return view('livewire.image-carousel', [
                'project_images'  => $project_images
            ]);
        }
    }
}
