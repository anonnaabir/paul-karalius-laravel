<?php

namespace App\Http\Livewire;
use Jenssegers\Agent\Agent;

use Livewire\Component;

class Header extends Component
{
    public function render()
    {
        $agent = new Agent();
        return view('livewire.header',compact('agent'));
    }
}
