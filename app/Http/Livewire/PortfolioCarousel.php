<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Portfolio;

class PortfolioCarousel extends Component
{
    public function render()
    
    {   
        // $data = Portfolio::all();
        $data = Portfolio::orderBy('order')->get();
        $portfolio = array_column(json_decode($data, true),'image','id');
        return view('livewire.portfolio-carousel',[
            'portfolio' => $portfolio,
        ]);
    }
}
