<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Portfolio;

class PortfolioGrid extends Component
{
    public function render()
    {
        $data = Portfolio::all();
        $portfolio = array_column(json_decode($data, true),'image','id');
        return view('livewire.portfolio-grid',[
            'portfolio' => $portfolio,
        ]);
    }
}
