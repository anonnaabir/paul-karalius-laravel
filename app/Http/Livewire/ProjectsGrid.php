<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ProjectsGrid extends Component
{
    public function render()
    {
        return view('livewire.projects-grid');
    }
}
