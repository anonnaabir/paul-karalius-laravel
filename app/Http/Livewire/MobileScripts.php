<?php

namespace App\Http\Livewire;

use Livewire\Component;

class MobileScripts extends Component
{
    public function render()
    {
        return view('livewire.mobile-scripts');
    }
}
