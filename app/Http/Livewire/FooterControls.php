<?php

namespace App\Http\Livewire;

use Livewire\Component;

class FooterControls extends Component
{   
    public $project_name;
    
    public function render()
    {
        return view('livewire.footer-controls',[
            'project_name' => $this->project_name
        ]);
    }

}
