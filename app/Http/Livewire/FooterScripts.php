<?php

namespace App\Http\Livewire;

use Livewire\Component;

class FooterScripts extends Component
{
    public function render()
    {
        return view('livewire.footer-scripts');
    }
}
