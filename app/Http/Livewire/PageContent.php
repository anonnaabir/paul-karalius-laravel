<?php

namespace App\Http\Livewire;
use Request;
use Livewire\Component;

class PageContent extends Component
{
    public $single_page;
    
    public function render()
    {
        // return view('livewire.page-content');
        return view('livewire.page-content', [
            'request_url'  => Request::path()
        ]);
    }
}
