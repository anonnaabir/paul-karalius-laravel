import Swiper from 'swiper/bundle';
import 'swiper/css/bundle';
import $ from "jquery";

  const swiper = new Swiper('.mySwiper', {
    slidesPerView: 1,
    autoplay:false,
    // speed: 500,
    spaceBetween: 30,
    effect: "fade",
    fadeEffect: {
        crossFade: true
      },
    loop: true,
    navigation: {
      nextEl: '#carousel-right',
      prevEl: '#carousel-left',
    },
  });

      document.getElementById("carousel-left").style.cursor = "w-resize";
    document.getElementById("carousel-right").style.cursor = "e-resize";

  document.getElementById("left-carousel").addEventListener("click", function(){
      swiper.slidePrev();
  });

  document.getElementById("right-carousel").addEventListener("click", function(){
    swiper.slideNext();
});

  $('.image-grid').click(function() {
    swiper.slideTo(this.id);
  });