<livewire:header-scripts />
<title>{{$single_project->name}} - {{setting('site.title')}}</title>
    </head>
    <body>
@if ($agent->isMobile())
    <livewire:mobile-header />
    <livewire:image-grid :single_project="$single_project"/>
    <livewire:footer />
    @else
    <div x-data="{ 
        show: false,
        hide: true
    }">

    
    <div x-show="hide" class="h-screen overflow-hidden">
    <div class="flex">
        <livewire:header />
        <livewire:image-carousel :single_project="$single_project">
    </div>
    <livewire:footer-controls :project_name="$project_name" />
    <livewire:footer />
    </div>

    <div x-show="show" class="h-screen">
    <div class="flex">
        <livewire:header />
        <livewire:image-grid :single_project="$single_project">
    </div>
    <livewire:footer-controls />
    <livewire:footer />
    </div>

</div>
@endif
@if ($agent->isMobile())
<livewire:mobile-scripts />
@else
<livewire:footer-scripts />
@endif