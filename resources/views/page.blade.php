<livewire:header-scripts />
    <title>{{$single_page->title}} - {{setting('site.title')}}</title>
    </head>
    <body>
    @if ($agent->isMobile())
    <livewire:mobile-header />
    <livewire:page-content :single_page="$single_page"/>
    @else
    <div>
    <div class="flex">
        <livewire:header />
        <livewire:page-content :single_page="$single_page"/>
    </div>
    <livewire:footer />
</div>
@endif
@if ($agent->isMobile())
<livewire:mobile-scripts />
@else
<livewire:footer-scripts />
@endif