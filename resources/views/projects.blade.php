<livewire:header-scripts />
    <title>Projects - {{setting('site.title')}}</title>
    </head>
    <body>
@if ($agent->isMobile())
    <livewire:mobile-header />
    <livewire:image-grid />
    <livewire:footer />
    @else
<div>
<div class="flex">
<livewire:header />
<livewire:image-grid />
</div>
<livewire:footer />
</div>
@endif
@if ($agent->isMobile())
<livewire:mobile-scripts />
@else
<livewire:footer-scripts />
@endif
