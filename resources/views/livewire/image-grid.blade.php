<div x-show="show" class="laptop:grid desktop:grid mobile:block grid-cols-3 pt-10 pr-6 mobile:pr-0">
    @if (isset($data_source))
    @foreach ($data_source as $index => $data)
    <div>
    @if ($request_url == 'projects')
    <a href="{{Request::url().'/'.$data->url}}">
    <div class="relative">
    <img id="{{$data->id}}" class="object-cover object-center p-4 w-full h-96" src="{{asset('storage/'.$data->feature_image)}}">
    <div class="absolute top-4 bg-black image-overlay ml-4 laptop:opacity-0 mobile:opacity-70 laptop:hover:opacity-70 transition hover:ease-in duration-700 delay-100">
        <p class="text-center text-white text-base font-semibold relative top-40">{{$data->name}}</p>
        <p class="text-center text-white text-base font-semibold relative top-40">{{$data->client_name}}</p>
    </div>
    </div>
    </a>
    @else
    <img id="{{$index+1}}" class="image-grid object-cover object-center p-4 w-full mobile:h-full laptop:h-80 desktop:h-96 macbook:h-96" src="{{asset('storage/'.$data->image)}}"
    x-on:click="
    show = false,
    hide = true
    ">
    @endif
</div>
    @endforeach
    @else

    @foreach ($project_images as $project_image)
    <div>
    <img class="object-cover object-center p-4 w-full h-96" src="{{asset('storage/'.$project_image)}}"
    x-on:click="
    show = false,
    hide = true
    ">
</div>
@endforeach

@endif
</div>
