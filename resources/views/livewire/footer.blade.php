

        <div class="sticky bottom-0 mobile:m-0 footer-background laptop:pl-72 mobile:pl-6 laptop:pr-72 mobile:pr-6 laptop:pt-4 mobile:pt-4 laptop:pb-10 mobile:pb-4 z-50 font-light">
        <div class="inline">
        
        <p class="laptop:float-left mobile:float-none laptop:text-left mobile:text-center text-base">Copyright {{$copyright}} Paul Karalius</p>
        
        <div class="laptop:block mobile:hidden laptop:float-right mobile:float-none space-x-3">
        <a target="_blank" href="https://twitter.com/paulkaralius"><i class="fab fa-twitter"></i></a>
        <a target="_blank" href="https://www.instagram.com/paulkaralius/"><i class="fab fa-instagram"></i></a>
        </div>

        </div>
    </div>
