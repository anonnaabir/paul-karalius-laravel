<div class="md:sticky md:top-0 md:self-start block laptop:w-3/12 pt-14 pl-32 font-light">
    <div class="w-36 min-w-36">
    <a href="\"><img src="{{ Voyager::image(setting('site.logo'))}}"></a>
    </div>
    <p class="pt-10"><a href="\">Portfolio</a></p>
    <!-- <p class="pt-6"><a href="\projects">Projects</a></p> -->
    <p class="pt-6"><a href="\about">About</a></p>
    <p class="pt-6"><a href="\contact">Contact</a></p>
</div>