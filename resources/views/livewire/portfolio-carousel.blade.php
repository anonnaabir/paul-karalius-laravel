<div x-show="hide" id="portfolio-carousel" class="swiper mySwiper">
      <div class="swiper-wrapper">
      <div id="carousel-left" class="block absolute w-2/5 h-5/6 z-50 mt-12"></div>
      @foreach ($portfolio as $key => $image)
        <div class="swiper-slide">
        <img id="{{$key}}" class="object-center laptop:pt-12 desktop:pt-12 macbook:pt-12 p-24 w-full h-screen"  src="{{asset('storage/'.$image)}}"></div>
        @endforeach
      <div id="carousel-right" class="block absolute w-2/5 h-5/6 z-50 right-24 mt-12"></div>
      </div>
      </div>
