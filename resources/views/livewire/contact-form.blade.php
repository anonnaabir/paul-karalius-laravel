<div class="block space-x-3 text-2xl mt-6 mb-6 m-4">
        <a target="_blank" href="https://twitter.com/paulkaralius"><i class="fab fa-twitter"></i></a>
        <a target="_blank" href="https://www.instagram.com/paulkaralius/"><i class="fab fa-instagram"></i></a>
        </div>

        @if ($errors->any())
    <div class="bg-orange-100 border-l-4 border-orange-500 text-orange-700 p-4 mb-12" role="alert">
    <p class="font-bold">Submission Error</p>
            @foreach ($errors->all() as $error)
                <p>{{ $error }}</p>
            @endforeach
      </div>
@endif
<div class="block footer-background bg-slate-400 w-5/6 p-6 mb-6 font-extralight">
  <form name="pkg-contact-form" method="POST">
    @csrf
    <label for="name">Name</label>
    <div class="mt-6 mb-6">
    <input class="w-full border-rose-600" type="text" id="name" name="name">
    </div>
    
    <label for="email">Email</label>
    <div class="mt-6 mb-6">
    <input class="w-full" type="text" id="email" name="email">
    </div>

    <label for="message">Message</label>
    <div class="mt-6 mb-6">
    <textarea class="w-full" id="message" name="message" style="height:200px"></textarea>
    </div>

    <label for="captcha">Enter Captcha</label>
    <div class="mt-6 mb-6">
    {!! captcha_img() !!}
    <input class="w-full mt-4" type="text" id="captcha" name="captcha">
    </div>

    <div>
    <input type="submit" class="submit-button-color text-white font-light rounded pl-6 pr-6 pt-3 pb-3" value="Submit">
    </div>
    
  </form>
</div>


<!-- <div class="block w-full p-6 pt-24 -ml-6">
    <h2 class="text-2xl font-semibold">Subscribe</h2>
    <p>Sign up with your email address to receive news and updates.</p>
  <form name="pkg-contact-form" method="POST" data-netlify="true">
    <label for="email">Email</label>
    <div class="mt-6 mb-6">
    <input class="w-full" type="text" id="email" name="email">
    </div>

    <div>
    <input type="submit" class="submit-button-color text-white font-light rounded pl-6 pr-6 pt-3 pb-3" value="Submit">
    </div>
    
  </form>
</div> -->