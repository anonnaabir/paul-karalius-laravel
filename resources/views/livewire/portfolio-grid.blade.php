<div x-show="show" class="grid grid-cols-3 pt-10 pr-6">
    @foreach ($portfolio as $key => $image)
    <img id="{{$key}}" class="object-cover object-center p-4 w-full h-96" src="{{asset('storage/'.$image)}}"
    x-on:click="
    show = false,
    hide = true
    ">
    @endforeach
</div>
