    <div x-data="
    {
       menuopen: false 
    }
    ">
<div x-show="menuopen" class="block text-center text-white align-middle bg-black h-screen pt-20">
    <p class="pt-10"><a href="\">Portfolio</a></p>
    <!-- <p class="pt-6"><a href="\projects">Projects</a></p> -->
    <p class="pt-6"><a href="\about">About</a></p>
    <p class="pt-6"><a href="\contact">Contact</a></p>
    <button @click="menuopen = false" class="pt-6">Close Menu</button>
</div>
        <div class="p-6 pl-10 pr-10">
        <div class="inline">
        <img class="float-left w-14 h-14" src="{{ Voyager::image(setting('site.mobile_site_logo'))}}">
        <button @click="menuopen = true" class="float-right mt-4">MENU</button>
        </div>
    </div>

</div>