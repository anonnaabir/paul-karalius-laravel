<div class="laptop:block desktop:block mobile:block laptop:w-6/12 desktop:w-6/12 mobile:w-full -ml-8 mobile:ml-0 pt-24 pr-6 mobile:pr-6 pl-0 mobile:pl-6">
<h1 class="font-medium">{{ $single_page->title }}</h1>    
<div class="pt-8 pb-8 font-sans font-extralight">{!!html_entity_decode($single_page->content)!!}</div>
@if ($request_url == 'contact')
<livewire:contact-form />
@endif
</div>
