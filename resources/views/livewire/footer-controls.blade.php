<div x-show="hide" class="sticky bottom-28 self-start block max-w-lg pl-32">
    @if (isset($project_name))
    <p class="p-6 pl-0 font-semibold">{{$project_name}}</p>
    @endif
    <div class="flex mb-4 space-x-3.5">
    <div id="left-carousel" class="carousel-left"><svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M11 17l-5-5m0 0l5-5m-5 5h12" />
</svg></div>
<div id="right-carousel" class="carousel-right"><svg xmlns="http://www.w3.org/2000/svg" class="carousel-left h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M13 7l5 5m0 0l-5 5m5-5H6" />
</svg></div>
</div>
<button type="button" @click="
    show = true,
    hide = false
    " class="text-sm font-light">Show Thumbnail</button>
    </div>
