<livewire:header-scripts />
<div class="flex h-screen text-center">
  <div class="m-auto">
    <img class="ml-28 w-2/5 h-2/5" src="{{ Voyager::image(setting('site.mobile_site_logo'))}}">
    <h1 class="p-4 text-xl mt-6 font-light">Thank you for contacting me</h1>
    <h1 class="p-2 text-xl mb-6 font-light">I will reply to you shortly</h1>
    
    <a href="\" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4">
    Back To Home
</a>
  </div>
</div>

<livewire:footer-scripts />