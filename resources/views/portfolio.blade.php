<livewire:header-scripts />
    <meta name="description" content="{{setting('site.description')}}">
    <title>{{setting('site.title')}} - {{setting('site.site_slogan')}}</title>
    </head>
    <body>
    @if ($agent->isMobile())
    <livewire:mobile-header />
    <livewire:image-grid />
    <livewire:footer />
    @else
    <div x-data="{ 
        show: false,
        hide: true
    }">

    <div x-show="hide" class="h-screen overflow-hidden">
    <div class="flex">
        <livewire:header />
        <livewire:portfolio-carousel />
    </div>
    <livewire:footer-controls />
    <livewire:footer />
    </div>

    <div x-show="show" class="h-screen">
    <div class="flex justify-center h-auto">
        <livewire:header />
        <livewire:image-grid />
    </div>
    <livewire:footer-controls />
    <livewire:footer />
    </div>

</div>
@endif
@if ($agent->isMobile())
<livewire:mobile-scripts />
@else
<livewire:footer-scripts />
@endif