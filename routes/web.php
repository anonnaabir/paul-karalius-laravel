<?php

use Illuminate\Support\Facades\Route;
use App\Models\Project;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'App\Http\Controllers\PortfolioController@render_portfolio');
Route::get('/projects', 'App\Http\Controllers\ProjectsController@render_projects');
Route::get('/projects/{project_url}', 'App\Http\Controllers\ProjectsController@single_project');
Route::get('/{page}', 'App\Http\Controllers\PageController@render_page');
Route::post('/contact', 'App\Http\Controllers\PageController@get_form_data');
Route::get('/contact', 'App\Http\Controllers\PageController@render_contact');
// Route::get('/projects/{project_url}', 'App\Http\Livewire\ImageGrid@get_single_project');

Route::group(['prefix' => 'secret/admin'], function () {
    Voyager::routes();
});

Route::get('/admin/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';
