const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    purge: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
    ],

    theme: {
        extend: {
            fontFamily: {
                sans: ['Open Sans','Nunito', ...defaultTheme.fontFamily.sans],
            },
        },
        screens: {
            'mobile': '360px',
            'tablet': '640px',
            'laptop': '1024px',
            'oldlaptop': {'max': '1280px'},
            'macbook': '1440px',
            'desktop': '1280px',
            ...defaultTheme.screens,
          },
    },

    variants: {
        extend: {
            opacity: ['disabled'],
        },
    },

    plugins: [require('@tailwindcss/forms')],
};
